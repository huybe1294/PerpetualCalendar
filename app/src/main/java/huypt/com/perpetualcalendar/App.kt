package huypt.com.perpetualcalendar

import android.app.Application

class App : Application() {
    companion object {
        lateinit var instances: App
    }

    override fun onCreate() {
        super.onCreate()
        instances = this
    }
}