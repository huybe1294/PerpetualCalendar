package huypt.com.perpetualcalendar.interfaces

import android.app.TimePickerDialog
import android.app.DatePickerDialog



/*
 * Created by huypt1 on 7/18/2018.
 */
interface TimeManagement {

    fun dialogDatePicker(onDateSetListener: DatePickerDialog.OnDateSetListener): TimeManagement

    fun showDatePickerDialog()

    fun dialogTimePicker(onTimeSetListener: TimePickerDialog.OnTimeSetListener): TimeManagement

    fun showTimePickerDialog()
}