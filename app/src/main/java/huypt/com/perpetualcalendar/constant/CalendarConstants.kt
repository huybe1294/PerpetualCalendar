package huypt.com.perpetualcalendar.constant

/*
 * Created by huypt1 on 7/9/2018.
 */
class CalendarConstants {
    companion object {
        const val TABLE_NAME_CALENDAR = "lich"
        const val COLUMN_ID = "id"
        const val COLUMN_DAY_OF_WEEK = "thu"
        const val COLUMN_LUNAR = "am_lich"
        const val COLUMN_SOLAR = "duong_lich"
        const val COLUMN_DAY = "ngay"
        const val COLUMN_MONTH = "thang"
        const val COLUMN_YEAR = "nam"
        const val COLUMN_LUCKY_HOUR = "gio_hoang_dao"
        const val COLUMN_UN_LUCKY_HOUR = "gio_hac_dao"
        const val COLUMN_EXIT_ORIENT = "huong_xuat_hanh"
        const val COLUMN_AGE_CONFLICT = "tuoi_xung_khac"
        const val COLUMN_GOOD_STAR = "sao_tot"
        const val COLUMN_BAD_STAR = "sao_xau"
        const val COLUMN_SHOULD = "nen_lam"
        const val COLUMN_NOT_SHOULD = "khong_nen_lam"
        const val COLUMN_LUCKY = "hoang_dao"

        const val SUNDAY = 0
        const val MONDAY = 1
        const val TUESDAY = 2
        const val WEDNESDAY = 3
        const val THURSDAY = 4
        const val FRIDAY = 5
        const val SATURDAY = 6

        const val STR_SATURDAY = "Thứ bảy"
        const val STR_SUNDAY = "Chủ nhật"
    }
}