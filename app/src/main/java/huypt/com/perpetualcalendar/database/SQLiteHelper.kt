package huypt.com.perpetualcalendar.database

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import huypt.com.perpetualcalendar.BuildConfig
import huypt.com.perpetualcalendar.constant.CalendarConstants
import huypt.com.perpetualcalendar.models.Calendar
import huypt.com.perpetualcalendar.utils.TimeUtils
import java.io.*
import java.text.SimpleDateFormat

class SQLiteHelper(private var context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        const val DATABASE_NAME = "lichvannien.db"
        const val DATABASE_VERSION = 1
        const val DATABASE_PATH = "data/data/" + BuildConfig.APPLICATION_ID + "/databases/"
    }

    private lateinit var mDatabase: SQLiteDatabase

    init {
        val dbExist = checkDatabase()
        if (dbExist) {
            openDatabase()
        } else {
            createDatabase()
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {}

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        if (newVersion > oldVersion)
            deleteDatabase()
    }

    /**
     * @param monthOfYear
     * @sample 01/1930
     * @return current month, last month, next month
     */
    @SuppressLint("Recycle")
    fun getDataInMonth(monthOfYear: String): List<Calendar> {
        val listCalendar = mutableListOf<Calendar>()
        val year = monthOfYear.substring(3)
        val lastMonth = (monthOfYear.substring(0, 2).toInt() - 1).toString()
        val nextMonth = (monthOfYear.substring(0, 2).toInt() + 1).toString()
        mDatabase = this.writableDatabase

        val sql = "SELECT * FROM " + CalendarConstants.TABLE_NAME_CALENDAR + " WHERE " +
                CalendarConstants.COLUMN_SOLAR + " LIKE '%" + monthOfYear + "'" + " OR " +
                CalendarConstants.COLUMN_SOLAR + " LIKE '%" + lastMonth + "/" + year + "'" + " OR " +
                CalendarConstants.COLUMN_SOLAR + " LIKE '%" + nextMonth + "/" + year + "'"

        try {
            val cursor: Cursor = mDatabase.rawQuery(sql, null)

            /**
             * @param   id, dayOfWeek, lunar, solar, day, month, year, luckyHour, unLuckyHour, exitOrient,
             *          ageConflict, goodStar, badStar, should, not_should, lucky
             */
            if (cursor.moveToFirst()) {
                do {
                    val calendar = Calendar(cursor.getInt(cursor.getColumnIndex(CalendarConstants.COLUMN_ID)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_DAY_OF_WEEK)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_LUNAR)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_SOLAR)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_DAY)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_MONTH)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_YEAR)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_LUCKY_HOUR)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_UN_LUCKY_HOUR)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_EXIT_ORIENT)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_AGE_CONFLICT)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_GOOD_STAR)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_BAD_STAR)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_SHOULD)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_NOT_SHOULD)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_LUCKY)))

                    listCalendar.add(calendar)
                } while (cursor.moveToNext())
            }

            cursor.close()
            this.close()
        } catch (e: SQLiteException) {
            e.printStackTrace()
        }
        return listCalendar
    }

    @SuppressLint("Recycle", "SimpleDateFormat")
    fun getDataInDay(): Calendar? {
        var calendar: Calendar? = null
        mDatabase = this.writableDatabase

        val dateFormat = SimpleDateFormat("dd/MM/yyyy")
        val date = TimeUtils.getNowString(dateFormat)

        val sql = "SELECT * FROM " + CalendarConstants.TABLE_NAME_CALENDAR + " WHERE " +
                CalendarConstants.COLUMN_SOLAR + " LIKE '%" + date + "'"

        try {
            val cursor: Cursor = mDatabase.rawQuery(sql, null)

            /**
             * @param   id, dayOfWeek, lunar, solar, day, month, year, luckyHour, unLuckyHour, exitOrient,
             *          ageConflict, goodStar, badStar, should, not_should, lucky
             */
            if (cursor.moveToFirst()) {
                do {
                    calendar = Calendar(cursor.getInt(cursor.getColumnIndex(CalendarConstants.COLUMN_ID)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_DAY_OF_WEEK)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_LUNAR)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_SOLAR)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_DAY)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_MONTH)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_YEAR)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_LUCKY_HOUR)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_UN_LUCKY_HOUR)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_EXIT_ORIENT)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_AGE_CONFLICT)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_GOOD_STAR)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_BAD_STAR)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_SHOULD)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_NOT_SHOULD)),
                            cursor.getString(cursor.getColumnIndex(CalendarConstants.COLUMN_LUCKY)))

                } while (cursor.moveToNext())
            }

            cursor.close()
            this.close()
        } catch (e: SQLiteException) {
            e.printStackTrace()
        }
        return calendar
    }

    //create a empty database on the system
    private fun createDatabase() {
        val dbExist = checkDatabase()
        if (dbExist) {
            Log.v("SQLiteHelper", "database exist")
        }

        val dbExist1 = checkDatabase()
        if (!dbExist1) {
            this.readableDatabase
            try {
                this.close()
                copyDatabase()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    //check database already exist or not
    private fun checkDatabase(): Boolean {
        var dbCheck = false
        try {
            val myPath = DATABASE_PATH + DATABASE_NAME
            val dbFile = File(myPath)
            dbCheck = dbFile.exists()
        } catch (e: SQLiteException) {
            e.printStackTrace()
        }
        return dbCheck
    }

    //copy database from local assets-folder to the just created empty database in the system folder
    @Throws(IOException::class)
    private fun copyDatabase() {
        try {
            val mInput: InputStream = context?.assets?.open(DATABASE_NAME)!!
            val outFileName = DATABASE_PATH + DATABASE_NAME
            val mOutput: OutputStream = FileOutputStream(outFileName)
            val mBuffer = ByteArray(2024)
            var length: Int

            do {
                length = mInput.read(mBuffer)
                if (length < 1) break
                mOutput.write(mBuffer, 0, length)
            } while (true)

            mOutput.flush()
            mOutput.close()
            mInput.close()

        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    //open database
    @Throws(SQLiteException::class)
    private fun openDatabase() {
        mDatabase = SQLiteDatabase.openDatabase((DATABASE_PATH + DATABASE_NAME), null, SQLiteDatabase.OPEN_READWRITE)
    }

    //close database
    @Synchronized
    @Throws(SQLiteException::class)
    private fun closeDatabase() {
        mDatabase.close()
        super.close()
    }

    //delete database
    private fun deleteDatabase() {
        val file = File(DATABASE_PATH + DATABASE_NAME)
        if (file.exists()) {
            file.delete()
        }
    }
}