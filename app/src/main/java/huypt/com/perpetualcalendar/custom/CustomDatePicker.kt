package huypt.com.perpetualcalendar.custom

import android.annotation.TargetApi
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.DialogFragment
import android.app.TimePickerDialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.widget.DatePicker
import huypt.com.perpetualcalendar.interfaces.TimeManagement
import java.util.*
import android.widget.Toast




/*
 * Created by huypt1 on 7/18/2018.
 */
class CustomDatePicker: DialogFragment() {

    private val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, month, day ->
        Toast.makeText(activity, "selected date is " + view.year +
                " / " + (view.month + 1) +
                " / " + view.dayOfMonth, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        return DatePickerDialog(activity, dateSetListener, year, month, day)
    }

}