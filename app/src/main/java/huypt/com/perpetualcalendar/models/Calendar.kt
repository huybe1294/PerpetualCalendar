package huypt.com.perpetualcalendar.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/*
 * Created by huypt1 on 7/9/2018.
 */

@Parcelize
data class Calendar(var id: Int,
                    var dayOfTheWeek: String,
                    var lunar: String,
                    var solar: String,
                    var day: String,
                    var month: String,
                    var year: String,
                    var luckyHour: String,
                    var unLuckyHour: String,
                    var exitOrient: String,
                    var ageConflict: String,
                    var goodStar: String,
                    var badStar: String,
                    var should: String,
                    var notShould: String,
                    var lucky: String) : Parcelable