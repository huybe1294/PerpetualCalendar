package huypt.com.perpetualcalendar.mudule.daily_calendar

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import huypt.com.perpetualcalendar.constant.Key
import huypt.com.perpetualcalendar.models.Calendar

class DailyCalendarAdapter(fm: FragmentManager?, private var calendar: Calendar?) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        val fragment = DailyCalendarPageFragment()
        val args = Bundle()
        args.putParcelable(Key.KEY_CALENDAR, calendar)
        fragment.arguments = args
        return fragment
    }

    override fun getCount() = 20
}