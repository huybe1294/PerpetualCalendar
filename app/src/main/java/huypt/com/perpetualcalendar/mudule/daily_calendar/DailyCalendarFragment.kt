package huypt.com.perpetualcalendar.mudule.daily_calendar

import android.os.Bundle
import com.ToxicBakery.viewpager.transforms.RotateDownTransformer
import huypt.com.kotlinandroid.base.BaseFragment
import huypt.com.perpetualcalendar.App
import huypt.com.perpetualcalendar.R
import huypt.com.perpetualcalendar.database.SQLiteHelper
import huypt.com.perpetualcalendar.models.Calendar
import kotlinx.android.synthetic.main.fragment_daily_calendar.*

class DailyCalendarFragment : BaseFragment() {

    private lateinit var mDatabase: SQLiteHelper
    private var calendar: Calendar? = null

    override fun initData(bundle: Bundle?) {}

    override fun bindLayout() = R.layout.fragment_daily_calendar

    override fun initView() {
        mDatabase = SQLiteHelper(App.instances)

//        view_pager_daily_cal.setPageTransformer(true, RotateDownTransformer())
        getData()
    }

    private fun getData() {
        calendar = mDatabase.getDataInDay()

        val mAdapter = DailyCalendarAdapter(activity?.supportFragmentManager, calendar)
        view_pager_daily_cal.adapter = mAdapter
    }
}