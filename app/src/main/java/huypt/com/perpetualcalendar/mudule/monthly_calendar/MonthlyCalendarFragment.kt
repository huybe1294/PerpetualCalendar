package huypt.com.perpetualcalendar.mudule.daily_calendar

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import com.bumptech.glide.Glide
import huypt.com.kotlinandroid.base.BaseFragment
import huypt.com.perpetualcalendar.App
import huypt.com.perpetualcalendar.R
import huypt.com.perpetualcalendar.constant.CalendarConstants
import huypt.com.perpetualcalendar.database.SQLiteHelper
import huypt.com.perpetualcalendar.models.Calendar
import huypt.com.perpetualcalendar.utils.TimeUtils
import kotlinx.android.synthetic.main.fragment_monthly_calendar.*
import java.text.SimpleDateFormat

class MonthlyCalendarFragment : BaseFragment() {

    private var listCalendar = mutableListOf<Calendar>()
    private lateinit var mDatabase: SQLiteHelper
    private lateinit var monthOfYear: String
    private var indexFirst: Int = 0 // vị trí ngày đầu tiên tháng hiện tại trong list
    private var indexLast: Int = 0 // vị trí ngày cuối cùng tháng hiện tại trong list
    private var today: Int = 0  // vị trí ngày hiện tại trong list

    override fun initData(bundle: Bundle?) {}

    override fun bindLayout() = R.layout.fragment_monthly_calendar

    override fun initView() {
        mDatabase = SQLiteHelper(App.instances)

        Glide.with(context).load(R.drawable.bg_noi_dung_0).into(img_bg)

        getData()
    }

    /**
     * get data from assets on list Calendar
     * set adapter to gridView
     */
    @SuppressLint("SimpleDateFormat")
    private fun getData() {
        val dateFormat = SimpleDateFormat("MM/yyyy")
        monthOfYear = TimeUtils.getNowString(dateFormat)

        val listCalendarTemp = mDatabase.getDataInMonth(monthOfYear)
        listCalendar.addAll(listCalendarTemp)

        val mAdapter = MonthlyCalendarAdapter(context!!, filterListCalendar(), indexFirst, indexLast, today)
        grid_calendar.adapter = mAdapter
    }

    /**
     * return 42 day matches GridView
     */
    @SuppressLint("SimpleDateFormat")
    private fun filterListCalendar(): List<Calendar> {

        /**
         * @see CalendarConstants.MONDAY
         * ----
         * @see CalendarConstants.SUNDAY
         */
        val firstDayOfCurrentMonth = TimeUtils.getFirstDayOfMonth()

        val dayNumberOfMonth = TimeUtils.getNumberOfDayInTheMonth() // number of days in the month (28,29,30,31)
        val currentMonth = TimeUtils.getNowString(SimpleDateFormat("MM/yyyy")) //return first day of current month with pattern "MM/yyyy"
        val listCalendar = mutableListOf<Calendar>()
        val indexFirstOfCurrentMonth = this.listCalendar.indexOfFirst { it.solar.contains(currentMonth) } // index ngày đấu tiên của tháng hiện tại trong list
        today = this.listCalendar.indexOfFirst { it.solar.contains(TimeUtils.getNowString(SimpleDateFormat("dd/MM/yyyy"))) } - TimeUtils.getNumberOfDayInLastMonth() // index ngày  hiện tại trong list

        when (firstDayOfCurrentMonth) {
            CalendarConstants.MONDAY -> {
                for (item in indexFirstOfCurrentMonth..indexFirstOfCurrentMonth + 41) {
                    listCalendar.add(this.listCalendar[item])
                }
                indexFirst = 0
                today += 1
            }

        //1+n+(42-n-1)
            CalendarConstants.TUESDAY -> {
                for (item in indexFirstOfCurrentMonth - 1..indexFirstOfCurrentMonth + 40) {
                    listCalendar.add(this.listCalendar[item])
                }
                indexFirst = 1
                today += 2
            }
        //2+n+(42-n-2)
            CalendarConstants.WEDNESDAY -> {
                for (item in indexFirstOfCurrentMonth - 2..indexFirstOfCurrentMonth + 49) {
                    listCalendar.add(this.listCalendar[item])
                }
                indexFirst = 2
                today += 3
            }

            CalendarConstants.THURSDAY -> {
                for (item in indexFirstOfCurrentMonth - 3..indexFirstOfCurrentMonth + 38) {
                    listCalendar.add(this.listCalendar[item])
                }
                indexFirst = 3
                today += 4
            }

            CalendarConstants.FRIDAY -> {
                for (item in indexFirstOfCurrentMonth - 4..indexFirstOfCurrentMonth + 37) {
                    listCalendar.add(this.listCalendar[item])
                }
                indexFirst = 4
                today += 5
            }

            CalendarConstants.SATURDAY -> {
                for (item in indexFirstOfCurrentMonth - 5..indexFirstOfCurrentMonth + 36) {
                    listCalendar.add(this.listCalendar[item])
                }
                indexFirst = 5
                today += 6
            }

            CalendarConstants.SUNDAY -> {
                for (item in indexFirstOfCurrentMonth - 6..indexFirstOfCurrentMonth + 35) {
                    listCalendar.add(this.listCalendar[item])
                }
                indexFirst = 6
                today += 7
            }
        }
        indexLast = indexFirst + dayNumberOfMonth
        return listCalendar
    }
}