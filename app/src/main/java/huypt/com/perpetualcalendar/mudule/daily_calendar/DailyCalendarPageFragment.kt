package huypt.com.perpetualcalendar.mudule.daily_calendar

import android.annotation.SuppressLint
import android.os.AsyncTask
import android.os.Bundle
import com.bumptech.glide.Glide
import huypt.com.kotlinandroid.base.BaseFragment
import huypt.com.perpetualcalendar.R
import huypt.com.perpetualcalendar.constant.Key
import huypt.com.perpetualcalendar.models.Calendar
import huypt.com.perpetualcalendar.utils.TimeUtils
import kotlinx.android.synthetic.main.fragment_daily_calendar_page.*
import java.text.SimpleDateFormat

@Suppress("UNREACHABLE_CODE")
class DailyCalendarPageFragment : BaseFragment() {
    private var calendar: Calendar? = null

    override fun bindLayout() = R.layout.fragment_daily_calendar_page

    override fun initData(bundle: Bundle?) {
        calendar = bundle?.getParcelable(Key.KEY_CALENDAR)
    }

    @SuppressLint("SetTextI18n")
    override fun initView() {
        Glide.with(context).load(R.drawable.bg_0).into(img_bg)
        if (calendar != null) {
            TimerTask().execute() //thời gian
            txt_day.text = TimeUtils.filterDay(calendar?.solar).toString() //ngày dương
            txt_text_day.text = calendar?.day // thứ
            txt_lunar.text = TimeUtils.filterDay(calendar?.lunar).toString() // ngày âm
            txt_lunar_month.text = "Tháng ${TimeUtils.filterMonth(calendar?.lunar)}"
            txt_day_2.text = "Ng. ${calendar?.day}"
            txt_month_2.text = "Th. ${calendar?.month}"
            txt_year.text = "Năm ${calendar?.year
            }"
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class TimerTask : AsyncTask<Void, String, String>() {
        @SuppressLint("SimpleDateFormat")
        override fun doInBackground(vararg p0: Void?): String {
            var time: String
            var hour: String
            while (true) {
                time = TimeUtils.getNowString(SimpleDateFormat("HH : mm"))
                hour = TimeUtils.getHour()
                Thread.sleep(1000)
                publishProgress(time, hour)
            }
        }

        override fun onProgressUpdate(vararg values: String?) {
            super.onProgressUpdate(*values)
            txt_time.text = values[0]
            txt_hour.text = values[1]
        }
    }
}