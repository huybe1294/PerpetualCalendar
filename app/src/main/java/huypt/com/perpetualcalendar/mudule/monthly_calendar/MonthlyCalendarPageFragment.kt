package huypt.com.perpetualcalendar.mudule.daily_calendar

import android.os.Bundle
import huypt.com.kotlinandroid.base.BaseFragment
import huypt.com.perpetualcalendar.R

class MonthlyCalendarPageFragment : BaseFragment() {
    override fun initData(bundle: Bundle?) {}

    override fun bindLayout() = R.layout.fragment_daily_calendar_page

    override fun initView() {}
}