package huypt.com.perpetualcalendar.mudule.daily_calendar

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import huypt.com.perpetualcalendar.R
import huypt.com.perpetualcalendar.constant.CalendarConstants
import huypt.com.perpetualcalendar.models.Calendar
import huypt.com.perpetualcalendar.utils.TimeUtils

class MonthlyCalendarAdapter(var context: Context,
                             private var listCalendar: List<Calendar>,
                             private var indexFirst: Int,
                             private var indexLast: Int,
                             private var today: Int) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val viewHolder: ViewHolder
        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_day_of_month, parent, false)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        viewHolder.txtDateSolar.text = TimeUtils.filterDay(listCalendar[position].solar).toString()
        viewHolder.txtDateSLunar.text = TimeUtils.filterDay(listCalendar[position].lunar).toString()

        if (listCalendar[position].dayOfTheWeek == CalendarConstants.STR_SATURDAY || listCalendar[position].dayOfTheWeek == CalendarConstants.STR_SUNDAY) {
            viewHolder.txtDateSolar.setTextColor(Color.RED)
            viewHolder.txtDateSLunar.setTextColor(Color.RED)
        }

        if (position < indexFirst || position >= indexLast) {
            viewHolder.txtDateSolar.setTextColor(Color.GRAY)
            viewHolder.txtDateSLunar.setTextColor(Color.GRAY)
        }

        if (position == today) {
            viewHolder.viewBorder.visibility = View.VISIBLE
        }

        return view
    }

    override fun getItem(position: Int) = listCalendar[position]

    override fun getItemId(position: Int) = listCalendar[position].id.toLong()

    override fun getCount() = listCalendar.size

    inner class ViewHolder(view: View?) {
        val txtDateSolar = view?.findViewById(R.id.txt_date_solar) as TextView
        val txtDateSLunar = view?.findViewById(R.id.txt_date_lunar) as TextView
        val viewBorder = view?.findViewById(R.id.view_border) as TextView
    }

}