package huypt.com.perpetualcalendar.mudule.horoscope

import android.app.DialogFragment
import android.os.Bundle
import butterknife.OnClick
import huypt.com.kotlinandroid.base.BaseFragment
import huypt.com.perpetualcalendar.R
import huypt.com.perpetualcalendar.custom.CustomDatePicker
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.toast

/*
 * Created by huypt1 on 7/18/2018.
 */
class HoroscopeFragment : BaseFragment() {
    override fun initData(bundle: Bundle?) {
    }

    override fun bindLayout() = R.layout.fragment_horoscope

    override fun initView() {
    }

    @OnClick(R.id.btn_date_picker)
     fun datePicker() {
        longToast("pick")
        val dialogFragment = CustomDatePicker()
        dialogFragment.show(activity?.fragmentManager, "date picker")
    }
}