package huypt.com.perpetualcalendar.activities

import android.annotation.SuppressLint
import android.os.Bundle
import butterknife.OnClick
import huypt.com.kotlinandroid.base.BaseActivity
import huypt.com.perpetualcalendar.App
import huypt.com.perpetualcalendar.R
import huypt.com.perpetualcalendar.database.SQLiteHelper
import huypt.com.perpetualcalendar.models.Calendar
import huypt.com.perpetualcalendar.mudule.daily_calendar.DailyCalendarFragment
import huypt.com.perpetualcalendar.mudule.daily_calendar.MonthlyCalendarAdapter
import huypt.com.perpetualcalendar.mudule.daily_calendar.MonthlyCalendarFragment
import huypt.com.perpetualcalendar.mudule.horoscope.HoroscopeFragment
import huypt.com.perpetualcalendar.utils.TimeUtils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_monthly_calendar.*
import org.jetbrains.anko.longToast
import java.text.SimpleDateFormat

class MainActivity : BaseActivity() {

    override fun initData(bundle: Bundle?) {}

    override fun bindLayout() = R.layout.activity_main

    override fun getContainerId() = R.id.container

    @SuppressLint("SimpleDateFormat")
    override fun initView() {
        navigation_bar.enableItemShiftingMode(false)
        navigation_bar.enableShiftingMode(false)

        switchScreenOnContainer(DailyCalendarFragment())
        navigation_bar.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_day -> {

                    //comment
                    //comment2
                    switchScreenOnContainer(DailyCalendarFragment())
                    true
                }
                R.id.nav_month -> {
                    switchScreenOnContainer(MonthlyCalendarFragment())
                    true
                }
                R.id.nav_change_date -> {
                    true
                }
                R.id.nav_tu_vi -> {
                    switchScreenOnContainer(HoroscopeFragment())
                    true
                }
                R.id.nav_more -> {
                    true
                }
                else -> false
            }
        }
    }
}
